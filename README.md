# Restful Benchmarks

Test difference framework performance

## What is this

* Testing difference language with same logic

## Test case

### Url: `/users`

* Create user object
* Return user with json

## Build

See subfolder `readme.md`

## Testing Environment

|  |  |
| ---- | ---- |
| CPU | i5-4460 3.20GHz |
| RAM | 16GB |
| Golang | go version go1.10.2 windows/amd64 |
| Java | openjdk version "10" 2018-03-20 |
| Nodejs | v9.9.0 |
| PHP | PHP 7.2.7 (cli) (built: Jun 19 2018 23:13:48) ( NTS MSVC15 (Visual C++ 2017) x64 |
| Python | 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:06:47) [MSC v.1914 32 bit (Intel)] |
| Jmeter | 4.0 r1823414 |

## Result

![responseTime](/jmeter/responseTime.png)

![responseTimesOverTime](/jmeter/responseTimesOverTime.png)

## TODO (No target timeline)

[] Ruby on Rails

[] Add more test case

[] Ensure no another code executed (e.g: interceptor)

## Contribution

Fork it.
