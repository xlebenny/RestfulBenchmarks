package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type user struct {
	FirstName string
	LastName  string
}

func main() {
	router := gin.New()
	router.GET("/users", func(c *gin.Context) {
		databaseUsers := []user{
			{FirstName: "Tai Man", LastName: "Chan"},
			{FirstName: "Benny", LastName: "Leung"},
		}

		c.JSON(http.StatusOK, databaseUsers)
	})
	router.Run(":9000")
}
