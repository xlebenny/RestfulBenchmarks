from flask import Flask
import json

app = Flask(__name__)

@app.route('/users')
def getAllUsers():
    usersFromDatabase = [
        UserModels("Tai Man", "Chan"),
        UserModels("Benny", "Leung")
    ]

    return json.dumps(usersFromDatabase, default=obj_dict)

class UserModels():
    firstName = ""
    lastName = ""

    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName

def obj_dict(obj):
    return obj.__dict__

if __name__ == "__main__":
    app.run()
