import static spark.Spark.*;
import com.google.gson.Gson;

public class Main {
    public static final Gson gson = new Gson();

    public static void main(String[] args) {
        get("/users", (req, res) -> {
            User databaseUsers[] = new User[] {
                new User("Tai Man", "Chan"),
                new User("Benny", "Leung"),
            };

            return databaseUsers;
        }, gson::toJson);
    }
}
