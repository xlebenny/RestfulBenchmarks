import * as express from "express"

import { UserModel } from "../models/UserModel"

const router = express.Router()

router.get('/', (req: express.Request, res: express.Response) => {
    let databaseUsers: UserModel[] = [
        { firstName: "Tai Man", lastName: "Chan" },
        { firstName: "Benny", lastName: "Leung" },
    ]
    res.json(databaseUsers);
})

export const UserController = router