import * as express from "express"

import { UserController } from "./controllers/UserController"

const app = express();
const port = process.env.PORT || 3000;

app.use('/users', UserController)

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`)
})