package controllers;

import org.springframework.web.bind.annotation.*;

import models.*;

@RestController
@RequestMapping("users")
public class UserController {
    @GetMapping("")
    public User[] getAllUsers() {
        User databaseUsers[] = new User[] {
            new User("Tai Man", "Chan"),
            new User("Benny", "Leung"),
        };
        return databaseUsers;
    }
}