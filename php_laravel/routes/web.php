<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

class User {
	public $firstName = "";
	public $lastName = "";

	function __construct($firstName, $lastName) {
		$this->firstName = $firstName;
		$this->lastName = $lastName;
	}
}


Route::get('/users', function()
{
	$databaseUsers = [
		new User("Tai Man", "Chan"),
		new User("Benny", "Leung"),
	];

	return response()->json($databaseUsers, 200);
});
